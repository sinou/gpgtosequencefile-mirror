package edu.ufl.cise;

import java.io.File;

import com.sleepycat.je.DatabaseException; 
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.StoreConfig;

import java.io.FileNotFoundException;

public class SimpleStorePut {

    

//	private static File envHome = new File("./"+fileName);

    private Environment envmnt;
    private EntityStore store;
    private SimpleDA sda; 
//    private String fileName;
    File envHome = null;
    public SimpleStorePut(String fileName) {
    	super();
    	envHome = new File("./"+fileName);
		try {
			setup();
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
	}
    

   // The setup() method opens the environment and store
    // for us.
    public void setup()
        throws DatabaseException {

        EnvironmentConfig envConfig = new EnvironmentConfig();
        StoreConfig storeConfig = new StoreConfig();

        envConfig.setAllowCreate(true);
        //envConfig.setInitializeCache(true);
        envConfig.setCacheSize(8192);
        storeConfig.setAllowCreate(true);

        try {
            // Open the environment and entity store
            envmnt = new Environment(envHome, envConfig);
            store = new EntityStore(envmnt, "EntityStore", storeConfig);
        } catch (Exception fnfe) {
            System.err.println("setup(): " + fnfe.toString());
            System.exit(-1);
        }
    } 

    // Close our environment and store.
    public void shutdown()
        throws DatabaseException {

        store.close();
        envmnt.close();
    } 


    private void run(String key, String value)
        throws DatabaseException {

//        setup();

        // Open the data accessor. This is used to store
        // persistent objects.
        sda = new SimpleDA(store);

        // Instantiate and store some entity classes
        SimpleEntityClass sec1 = new SimpleEntityClass();

        sec1.setpKey(key);
        sec1.setsKey(value);


        sda.pIdx.put(sec1);

//        sda.close();

//        shutdown();
    } 
    
    public void end() throws DatabaseException{
    	sda.close();

        shutdown();
    }

    public void triggerPut(String args[]) {
//        SimpleStorePut ssp = new SimpleStorePut();
        try {
            this.run(args[0], args[1]);
        } catch (DatabaseException dbe) {
            System.err.println("SimpleStorePut: " + dbe.toString());
            dbe.printStackTrace();
        } catch (Exception e) {
            System.out.println("Exception: " + e.toString());
            e.printStackTrace();
        } 
    } 

}