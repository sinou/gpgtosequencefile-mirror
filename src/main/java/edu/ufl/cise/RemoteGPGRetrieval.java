package edu.ufl.cise;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.compress.compressors.xz.XZCompressorInputStream;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TIOStreamTransport;
import org.apache.thrift.transport.TTransportException;

import streamcorpus.Sentence;
import streamcorpus.StreamItem;
import streamcorpus.Token;

import java.io.File;
import java.io.FileWriter;

public class RemoteGPGRetrieval {

	public static final String SDD_BASE_PATH = "/home/srini/Documents/gatordsr/gpg/";
	public static final String SDE_BASE_PATH = "/home/srini/Documents/gatordsr/gpg/";
	
	public static void main(String[] args) throws IOException {
		FileWriter fileWriter = null;
                ParserDemo pd = new ParserDemo();
                //pd.test();
		List<StreamItem> l = null;
		String fileName = "MAINSTREAM_NEWS-27-41a3496751204548a939df92e8e33487-4c536344841014a3291617a3b3afb4bd.sc.xz.gpg";
		System.out.println("--");
		l = getLocalStreams("2013-01-03-00", "MAINSTREAM_NEWS-27-41a3496751204548a939df92e8e33487-4c536344841014a3291617a3b3afb4bd.sc.xz.gpg");
			   
		 System.out.println(l.size());
		 System.out.println("--"); 
		try {
			for(int i = 0; i <l.size(); i ++ ){
				System.out.println("-----------" + i);
			  String fileNameAndDate = fileName + "2013-01-03-00"+"-"+i;//hardcoded for now
	    (new File("./"+fileNameAndDate)).mkdir();

			 //l = getStreams("2011-10-05-00", fileName);
			 
                         String content = l.get(i).body.getClean_visible();
                         if(content == null){
                        	 continue;
                         }
			 System.out.println(content.length());
			/* File newTextFile = new File("/home/srini/Documents/gatordsr/stream/1.txt");
            		 fileWriter = new FileWriter(newTextFile);
            		 fileWriter.write(content);
            		 fileWriter.close();*/
			 if(args[0].length() != 0){
			 	if(args[0].equals("TOK")){
			 	pd.demoTOK("/home/srini/Documents/gatordsr/stream/1.txt");
			 }
				if(args[0].equals("SENT")){
			 	pd.demoSENT(content, content.length(), fileNameAndDate);
			 }
				if(args[0].equals("POS")){
			 	pd.demoPOS("/home/srini/Documents/gatordsr/stream/1.txt");
			 }
				if(args[0].equals("NER")){
			 	pd.demoNER("/home/srini/Documents/gatordsr/stream/2.txt");
			 }
			 }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<StreamItem> getLocalStreams(String date, String fileName)
			throws IOException {
		File f = new File(SDD_BASE_PATH + date + "/" + fileName);
		if (f.exists())
			return getLocalStreams(SDD_BASE_PATH, date, fileName);
		else
			return getLocalStreams(SDE_BASE_PATH, date, fileName);

	}

	public static List<StreamItem> getLocalStreams(String basePath,
			String date, String fileName) throws IOException {
		String command = "gpg -q --no-verbose --no-permission-warning --trust-model always --output - --decrypt "
				+ basePath + date + "/" + fileName;
		System.out.println(command);
		InputStream is = FileProcessor.runBinaryShellCommand(command);
		XZCompressorInputStream xzis = new XZCompressorInputStream(is);
		TIOStreamTransport transport = new TIOStreamTransport(xzis);

		List<StreamItem> list = new LinkedList<StreamItem>();

		boolean exception = false;
		while (!exception) {
			try {
				transport.open();
				TBinaryProtocol protocol = new TBinaryProtocol(transport);

				int index = 0;

				StreamItem si = new StreamItem();
				if (protocol.getTransport().isOpen())
					si.read(protocol);
				list.add(si);
				// SIWrapper siw = new SIWrapper(day, hour, fileName, index,
				// si);
				index = index + 1;
			} catch (TTransportException e) {
				tTransportExceptionPrintString(e);
				exception = true;
			} catch (TException e) {
				e.printStackTrace();
			}
		}
		transport.close();
		return list;
	}

	/**
	 * Get the appropirate cause of exception string for TTransportException
	 * 
	 * @param e
	 */
	public static void tTransportExceptionPrintString(TTransportException e) {
		switch (e.getType()) {
		case TTransportException.ALREADY_OPEN:
			System.err.println("Error reading StreamItem: ALREADY_OPEN");
			break;
		case TTransportException.END_OF_FILE:
			// System.err.println("Error reading StreamItem: END_OF_FILE");
			break;
		case TTransportException.NOT_OPEN:
			System.err.println("Error reading StreamItem: NOT_OPEN");
			break;
		case TTransportException.TIMED_OUT:
			System.err.println("Error reading StreamItem: TIMED_OUT");
			break;
		case TTransportException.UNKNOWN:
			System.err.println("Error reading StreamItem: UNKNOWN");
			break;
		}
	}

	public static List<StreamItem> getStreams(String date, String fileName) {

		String command = "sshpass -p 'trecGuest' ssh trecGuest@sm321-01.cise.ufl.edu 'cat /home/srini/Documents/gatordsr/gpg/"
				+ date
				+ "/"
				+ fileName
				+ "' | gpg  --no-permission-warning --trust-model always --output - --decrypt - | xz --decompress";
		 System.out.println(command);

		InputStream is = FileProcessor.runBinaryShellCommand(command);
		TIOStreamTransport transport = new TIOStreamTransport(is);
		try {
			transport.open();
		} catch (TTransportException e1) {
			e1.printStackTrace();
		}
		TBinaryProtocol protocol = new TBinaryProtocol(transport);

		LinkedList<StreamItem> list = new LinkedList<StreamItem>();
		int index = 0;
		boolean exception = false;
		while (!exception) {
			StreamItem si = new StreamItem();
			try {
				si.read(protocol);
				if (si.getBody() != null
						&& si.getBody().getClean_visible() != null) {
					// System.out.println(si.getBody().getClean_visible()
					// .substring(0, 5));
				}
			} catch (Exception e) {
				exception = true;
				// System.err.println(e);
			}
			list.add(si);
			index = index + 1;
		}
		transport.close();
		return list;
	}

	/**
	 * Reads non encrypted si files and returns a list of them.
	 */
	public static List<StreamItem> readNonEncrypted(String fileName)
			throws IOException, TTransportException {
		InputStream is = new java.io.FileInputStream(new java.io.File(fileName));
		XZCompressorInputStream xzis = new XZCompressorInputStream(is);
		TIOStreamTransport transport = new TIOStreamTransport(xzis);
		TBinaryProtocol protocol = new TBinaryProtocol(transport);
		System.err.println("readNonEncrypted: " + fileName);
		transport.open();
		LinkedList<StreamItem> listSI = new LinkedList<StreamItem>();
		boolean exception = false;
		while (!exception) {
			try {
				StreamItem si = new StreamItem();
				si.read(protocol);
				listSI.add(si);
				System.out.println(si.getBody().getSentences().get("lingpipe")
						.get(0).getTokens().get(0));
			} catch (TTransportException e) {
				RemoteGPGRetrieval.tTransportExceptionPrintString(e);
				// e.printStackTrace();
				exception = true;
			} catch (TException e) {
				e.printStackTrace();
			}
		}
		transport.close();
		return listSI;

	}
}
