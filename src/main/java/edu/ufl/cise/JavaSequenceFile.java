package edu.ufl.cise;

import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;

import java.util.HashMap;

class JavaSequenceFile{

	private static String[] DATA = { "test", "string", "array" };
	
	public static void createSequenceFile(String args[]) throws IOException{
                DATA = args;
		HashMap map = new HashMap();
		 SequenceFile.Writer writer = null;
		 String uri = "/home/srini/data/1.seq";
		FileSystem fs = null;
		Configuration conf = null;
		Path path = null;
		try{
        	conf = new Configuration();
        	fs = FileSystem.get(URI.create( uri), conf);
		
        	path = new Path( uri);
        	IntWritable key = new IntWritable();
        	Text value = new Text();
                
        	
        	
            		writer = SequenceFile.createWriter( fs, conf, path, key.getClass(), value.getClass());
			
            		for (int i = 0; i < DATA.length; i ++) { 
			
		        key.set(i);
					        value.set( DATA[ i % DATA.length]);
			System.out.println( key + " - " + value);
			System.out.println();
		        //System.out.printf("[% s]\t% s\t% s\n", writer.getLength(), key, value); 
		        writer.append( key, value); } 
}catch(Exception e){System.out.println(e);}			
 finally  
			{ 

				IOUtils.closeStream( writer); 
							

                                fs.close();
			} 

	}



}
